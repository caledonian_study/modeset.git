一  makefile编译

编译步骤如下：
解压，加载交叉工具链到环境变量，make编译
![输入图片说明](https://images.gitee.com/uploads/images/2021/0317/153357_67f3d94f_1032644.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0317/153409_1197c88a_1032644.png "屏幕截图.png")
知识点
先看makefile

FLAGS=`pkg-config --cflags --libs libdrm`
FLAGS+=-Wall -O0 -g
FLAGS+=-D_FILE_OFFSET_BITS=32

all:
	$(CC) -o modeset modeset.c $(FLAGS)
	$(CC) -o modeset-double-buffered modeset-double-buffered.c $(FLAGS)
	$(CC) -o modeset-vsync modeset-vsync.c $(FLAGS)
	$(CC) -o modeset-atomic modeset-atomic.c $(FLAGS)
	
1 pkg-config得使用，加载libdrm库

2 加载了环境变量之后，编译
gcc改为  $(CC)


二  qmake编译
在虚拟机上面用qtcreat打开
![输入图片说明](https://images.gitee.com/uploads/images/2021/0317/153421_670b8dcc_1032644.png "屏幕截图.png")

然后单击编译运行就可以了。
知识点1qmake里面加pkg-config 

1 工程上单机右键，添加库

![输入图片说明](https://images.gitee.com/uploads/images/2021/0317/153431_cebc9ab7_1032644.png "屏幕截图.png")
